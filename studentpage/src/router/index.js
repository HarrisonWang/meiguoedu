import Vue from 'vue'
import Router from 'vue-router'
import Base from '../components/Base'
import VueScrollTo from 'vue-scrollto'
import VueResource from 'vue-resource'

Vue.use(VueScrollTo)
Vue.use(Router)
Vue.use(VueResource);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Base',
      component: Base
    }
  ]
})
