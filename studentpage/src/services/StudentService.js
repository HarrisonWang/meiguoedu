import Vue from 'vue';
export default {
  login() {
    // TODO: DEV USE ONLY delete after student page complete
    let body = { username: "ds@me.com", password: "Wjm211111!"};
    Vue.http.post('/auth/login',body).then(
      function (res) {
        sessionStorage.setItem('meiguo_user', res.body.userInfo.username);
        sessionStorage.setItem('nickname',res.body.userInfo.nickname);
        sessionStorage.setItem('role',res.body.userInfo.role);
      },
      function(err){
        error(err);
      });
  },

  logout(success,error) {
    Vue.http.get('/auth/logout').then(success, err => {error(err)});
  },

  getStudentDetail(studentEmail, success, error){
    if(studentEmail && success && error){
      Vue.http.get('/v1/students/' + encodeURI(studentEmail)).then(
        res => { success(res.data)},
        err => { error(err)}
      );
    }
  },

  getActivity(studentId, success, error){
    if(studentId&&success&&error){
      Vue.http.get('/v1/activities/'+studentId).then(
        res => { success(res.data)},
        err => { error(err)}
      );
    }
  },

  getTodo(studentId, success, error){
    if(studentId&&success&&error){
      Vue.http.get('/v1/todos/'+studentId).then(
        res => { success(res.data)},
        err => { error(err)}
      );
    }
  },

}
