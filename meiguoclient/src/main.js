import Vue from 'vue'
import App from './App'
import router from './router'
import 'materialize-css/dist/js/materialize.min.js'
import 'materialize-css/dist/css/materialize.min.css'
import 'jquery/dist/jquery'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import './assets/css/newLayout.css'
import './assets/css/badges.css'
import './assets/css/snackbar.css'

Vue.config.productionTip = false;
Vue.component('icon', Icon);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App,Icon }
})
