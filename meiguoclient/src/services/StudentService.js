import Vue from 'vue';


export default {
  login() {
    // TODO: DEV USE ONLY delete after student page complete
    let body = { username: "vp@me.com", password: "Wjm211111!"};
    // console.log(body);
    Vue.http.post('/auth/login',body).then(
      function (res) {
        sessionStorage.setItem('meiguo_user', res.body.userInfo.username);
        sessionStorage.setItem('nickname',res.body.userInfo.nickname);
        sessionStorage.setItem('role',res.body.userInfo.role);
      },
      function(err){
        console.log(err);
        error(err);
      });
  },

  createUser(username, password, role, success, error){
    if(username&& password&& role&& success&& error){
      let body = {
        username: username,
        password: password,
        userrole: role
      };
      Vue.http.post('/auth/signup', body).then(
        success,
        err => {error(err)}
      );
    }
  },

  deleteUser(username, success, error){
    if(username&& success&& error){
      Vue.http.delete('/v1/users/' + username).then(
        success,
        err => {error(err)}
      );
    }
  },

  logout(success,error) {
    Vue.http.get('/auth/logout')
        .then(
          success,
          err => {error(err)}
      );
  },

  getstudentSummary(success, error){
    Vue.http.get('/v1/students').then(
      res => {
        console.log(res.data);
        success(res.data)
      },
      err => { error(err)}
    );
  },

  getStudentDetail(studentId, success, error){
    if(studentId&&success&&error){
      Vue.http.get('/v1/students/'+studentId).then(
        res => { success(res.data)},
        err => { error(err)}
      );
    }
  },

  updateStudentData(studentId, updatefield, updatedata, success, error){
    if(studentId&& updatefield&& updatedata&& error){
      let body = {
        target: updatefield,
        data: updatedata
      };
      Vue.http.post('/v1/students/'+studentId, body).then(
        success,
        err => { error(err)}
      );
    }
  },

  bindStaff(studentId, staffEmail, success, error){
    if(studentId && staffEmail && error){
      let body = {
        action: 'bind',
        studentId: studentId,
        staffEmail: staffEmail,
      };
      Vue.http.post('/v1/relations/', body).then(
        data => { success(data) },
        err => { error(err) }
      );
    }
  },

  unbindStaff(studentId, staffEmail, success, error){
    if(studentId&& staffEmail&& error){
      let body = {
        action: 'unbind',
        studentId: studentId,
        staffEmail: staffEmail,
      };
      Vue.http.post('/v1/relations/', body).then(
        success,
        err => { error(err)}
      );
    }
  },

  getActivity(studentId, success, error){
    if(studentId&&success&&error){
      Vue.http.get('/v1/activities/'+studentId).then(
        res => { success(res.data)},
        err => { error(err)}
      );
    }
  },

  logActivity(payload, success, error){
    if(payload&& success&& error){
      Vue.http.post('/v1/activities/', payload).then(
        success,
        err => { error(err)}
      );
    }
  },

  removeOneBadge(studId,badgeId,success,error){
    console.log(studId,badgeId);
    if (studId && badgeId && success && error) {
      Vue.http.delete('/v1/students/'+studId+'/badges/'+badgeId)
        .then(success,err =>{error(err)})
        .catch(function (err) {
          console.log('server error',err);
        });
    }
  },

  addOneBadge(studId,badgeId,success,error) {
    if (studId && badgeId && success && error) {
      Vue.http.post('/v1/students/'+studId+'/badges/'+badgeId)
        .then(
          success,
            err =>{
            error(err)
          })
        .catch(function (err) {
          console.log('server error',err);
        });
    }
  },

  getTodo(studentId, success, error){
    if(studentId&&success&&error){
      Vue.http.get('/v1/todos/'+studentId).then(
        res => { success(res.data)},
        err => { error(err)}
      );
    }
  },

  logTodo(payload, success, error){
    // console.log(payload)
    if(payload&& success&& error){
      // payload.todoRole = payload.role;
      Vue.http.post('/v1/todos/', payload).then(
        success,
        err => { error(err)}
      );
    }
  },

  updateTodo(todoId, field, value, success, error){
    console.log(todoId,field,value,success,error);
    if(todoId&& field&& (value || value === false )&& success&& error){
      let payload = {
        field: field,
        value: value
      }
      Vue.http.post('/v1/todos/'+todoId, payload).then(
        success,
        err => { error(err)}
      );
    }
  },

  deleteTodo(todoId,success,error) {
    console.log(todoId);
    if (todoId && success && error) {
        Vue.http.delete('/v1/todos/'+todoId).then(
          success,
          err=>{ error(err)}
        );
    }
  },

  uploadImg(img,success,error,id) {
    console.log('upload image: id-->',id);
    Vue.http.post('/v1/portrait/'+(id ? id : ""),img).then(
      success,
      err => {error(err)}
    )
  }
}
