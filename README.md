# MeiguoEdu

## What is the project about?
This is a student information management system. We are going to use the most popular web technology to build a
user friendly web application. We focus on Nodejs, Vuejs and Postgresql database.

## Project File Structure
![file structure](https://raw.githubusercontent.com/JiamengWang/ImageStore/master/meiguoEdu_file_structure.png)
##### package.json
> This is the file where we define package dependencies for backend server. As long as your environment has NPM
installed, you can run command 'npm install' to download all dependencies into your local manchine. This is the
prerequisite before you start coding.

#### meiguoClient
> meiguoClient is child project under meiguoedu. This project including all front-end code we have.

> The core of express server basically speaking is a http server which processing client's request and response to 
it. Difference between front-end and back-end server is the message type in their response.

> Front-end server will focus on delivering static content like html, img, js script or css.

> Back-end server will focus on processing business logic and sending data back to client's web browser

#### meiguoParser
> meiguoParser is another child project which is aimed at processing and retrieving data from surveyMonkey. This
part is still under developing.

#### sql
> This folder stores sql commands to build tables for our project

#### router
> This folder stores all apis for our web application.

## External Link
1. [Vue.js](https://vuejs.org/index.html)
2. [Koa a web framework for node.js](https://koajs.com)
3. [Express - Node.js web application framework](http://expressjs.com)
4. [Materialize - a modern respective front-end framework based on Material Design](https://materializecss.com)
5. [vue-cli, A simple CLI for scaffolding Vue.js projects ](https://www.npmjs.com/package/vue-cli)