var express = require('express');
var router = express.Router();
var db = require('../query.js');
var acl = require('../utility/acl');
var file = require('../utility/fileUtility');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' ,name:'wjm'});
});

// STUDENT =============================
/**
 * who: ADMIN/PMC/DOE/FT
 * i: null
 * o: students summary based on who
 * */
router.get('/students',acl.isAuthorized("studentList", "get"),db.getstudentSummary);

/** who: ADMIN/PMC/DOE
 * i: student id (uuid)
 * o: studnet detail info including bio+to_do+activity+related_staff_info
 * */
router.get('/students/:studentinfo',acl.isAuthorized("studentDetail", "get"),db.getStudentDetail);

// router.post('/students',db.createStudent);

router.post('/students/:id',acl.isAuthorized("studentInfo", "update"),db.updateStudent);


// STAFF =================================
router.get('/staffs',acl.isAuthorized("staffList", "get"),db.getAllStaffs);

router.get('/staffs/:id',acl.isAuthorized("staffInfo", "get"),db.getStaffDetail);

// router.post('/staffs',db.createStaff);


// USER ==================================
router.post('/users',acl.isAuthorized("user", "post"),db.createUser);
router.delete('/users/:username',acl.isAuthorized("user", "delete"),db.removeUser);


// RELATION ==============================
router.post('/relations',acl.isAuthorized("relation", "post"),db.relationHandler);


// ACTIVITY ===============================
router.get('/activities/:id',acl.isAuthorized("activity", "get"),db.getActivity);
router.post('/activities',acl.isAuthorized("activity", "post"),db.logActivity);

// TODOS ==================================
router.get('/todos/:id',acl.isAuthorized("todo", "get"),db.getTodo);
router.post('/todos',acl.isAuthorized("todo", "post"),db.logTodo);
router.post('/todos/:id',acl.isAuthorized("todo", "post"),db.updateTodo);
router.delete('/todos/:id',db.deleteTodo);//TODO: need acl authentication check

// BADGE ==========================
router.delete('/students/:studId/badges/:badgeId',acl.isAuthorized("badge", "delete"),db.removeOneBadge);
router.post('/students/:studId/badges/:badgeId',acl.isAuthorized("badge", "post"),db.addOneBadge);

// IMAGE ==========================
router.post('/portrait/',file.writePortrait);
router.get('/portrait/',file.getPortrait);
router.get('/portrait/:id',file.getIdPortrait);
router.post('/portrait/:id',file.writeIdPortrait);

module.exports = router;
