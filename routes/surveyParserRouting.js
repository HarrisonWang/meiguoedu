var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../config.yaml'), 'utf8'));

var jayson = require('jayson');
var surveyParser = jayson.client.http({
    port: config.PARSERPORT
});

router.head('/:id',function (req,res,next) {
    console.log('webhook head request',req.params);
    res.end('success');
});

router.get('/:id',function (req,res,next) {
    var args = req.body;
    args.id = req.params.id;
    surveyParser.request('parse',args, function(err, response) {
        if(err) throw err;
        // console.log(response.result);

        // we can insert data into db here???
        res.json(response.result);
    });
});

router.post('/:id',function (req,res,next) {
    var args = req.body;
    args.id = req.params.id;
    surveyParser.request('parse',args, function(err, response) {
        if(err) throw err;
        // console.log(response.result);
        console.log(response);
        // we can insert data into db here???
        // res.json(response.result);
    });
});

module.exports = router;
