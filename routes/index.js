var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../config.yaml'), 'utf8'));
// var redirectServer = http.createServer(express);

/**
 * @see https://www.npmjs.com/package/jsonwebtoken
 * */
/* GET home page. */
router.get(/(^\/$)|(^\/login$)/, function(req, res, next) {
    res.sendFile('login.html', {root: path.join(__dirname, '../public/login')});
});

router.get('/logout',function (req,res,next) {

    jwt.verify(req.cookies.jwt,cert,function (err,decode) {
        if(err) {
            console.log(err);
            res.redirect('/');
            res.end();
            return;
        }
        var token = jwt.sign({
            role:'TimeOut',
            sub:decode.sub,
            idt:Math.floor(Date.now() / 1000),
            exp:Math.floor(Date.now() / 1000) - 1000,
        }, cert);
        res.cookie('jwt',token,{httpOnly:true});
        res.json({
            code:'302',
            url:''
        });
    });
});

router.get('/privacy',function (req,res,next) {
    res.sendFile('privacypolicy.html', {root: path.join(__dirname, '../public/login')});
})

router.get('/admin',function (req,res,next) {
    validateRoleAndSendFile(res,req.cookies.jwt,'ADMIN','forAdmin/index.html');
});

router.get('/doe',function (req,res,next) {
    validateRoleAndSendFile(res,req.cookies.jwt,'DOE','/forAdmin/index.html');
    // res.end('request doe page');
});

router.get('/pmc',function (req,res,next) {
    validateRoleAndSendFile(res,req.cookies.jwt,'PMC','forAdmin/index.html');
    // res.end('request pmc page');
});

router.get('/ft',function (req,res,next) {
    validateRoleAndSendFile(res,req.cookies.jwt,'FT','forAdmin/index.html');
    // res.end('request pmc page');
});

router.get('/stud',function (req,res,next) {
    validateRoleAndSendFile(res,req.cookies.jwt,'STUD','forAdmin/index.html');
    // res.end('request student page');
});

router.get('/reset',function (req,res,next) {
    res.sendFile('resetPassword.html',{root: path.join(__dirname, '../public/resetpassword')})
});


var validateRoleAndSendFile = function (res,cookie,role,file) {
    // var options = {
    //     headers: {
    //         'x-timestamp': Date.now(),
    //         'x-sent': true
    //     }
    // };
    jwt.verify(cookie,config['JWT']['SECRET'],function (err,decode) {
        if (err) {
            console.log(err);
            res.redirect('/');
            res.end();
            return;
        }
        if (decode.role == role) {
            res.set({
                'Cache-Control': ['no-store','must-revalidate'],
                'Pragma': 'no-cache',
                'Expires': 0
            });
            res.sendFile(file,{root: path.join(__dirname, '../public')});
        } else {
            res.redirect('/');
            res.end();
        }
    });
}


module.exports = router;
