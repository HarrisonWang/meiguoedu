// this function is needed to avoid circular dependency between acl.js and query.js

var db = require('../query');
var acl = require('./acl');

function init(){
    db.getAllLoginCB(function(users){
        for(let i=0; i<users.length; i++){
            acl.addUserRoles(users[i].username, users[i].role);
        }
    }, function (err) {
        console.log(err);
    });
};

module.exports = {init:init};



