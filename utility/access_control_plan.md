# get /students/:id
    get a student detail
    user: student, [ft, pmc, doe] only see student bonding to, admin
    obj: studentDetail
    act: get

# get /activities/:id
    get activities related to a student
    user: student, ft, pmc, doe, admin
    obj: activity
    act: get
    
# get /todos/:id
    get todos related to a student
    user: student, ft, pmc, doe, admin
    obj: todo
    act: get
========================================================================
# get /students 
    get students summary list
    user: [ft, pmc, doe] only see student bonding to, admin
    obj: studentList
    act: get
    
# get /staffs
    get all staffs info
    user: ft, pmc, doe, admin
    obj: staffList
    act: get
    
# get /staffs/:id
    get a staff detail
    user: ft, pmc, doe, admin
    obj: staffInfo
    act: get
========================================================================
# post /activities
    add an activities
    user: pmc, doe, admin
    obj: activity
    act: post
    
# post /todos
    add todos
    user: pmc, doe, admin
    obj: todo
    act: post

# post /todos/:id
    update a todo
    user: pmc, doe, admin
    obj: todo
    act: post
======================================================================== 
# delete /students/:studId/badges/:badgeId
    remove a badge from a student
    user: doe, admin
    obj: badge
    act: delete
    
# post /students/:studId/badges/:badgeId
    add a badge to a student
    user:doe, admin
    obj: badge
    act: post
========================================================================
# post /students/:id
    update a student info
    user: admin
    obj: studentInfo
    act: update  
    
# post /users
    create a user
    user: admin ONLY
    obj: user
    act: post
    
# delete /users/:username
    delete a user
    user: admin ONLY
    obj: user
    act: delete
    
# post /relations
    bind/unbind staff from students
    user: admin
    obj: relation
    act: post
========================================================================    
# post /students
    create a student
    user: survey monkey ONLY
    
# post /staffs 
    create a staff
    user: survey monkey ONLY