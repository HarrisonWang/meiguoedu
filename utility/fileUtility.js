var fs = require('fs');
var path = require('path');
var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './img/') //TODO: remember to create img folder in project
    },
    filename: function (req, file, cb) {
        console.log("in filename:",file);
        cb(null, req.imgID+'.jpg')
    }
})
// var upload = multer({ storage: storage }).any();
var upload = multer({ storage: storage }).single('fileToUpload')


function writePortrait(req,res,next) {
    console.log('in multer');
    req.imgID = req.body.userID;
    upload(req,res,function (err) {
        if (err) {
            console.log('Error in multer: ',err);
            return next(err);
        }
        console.log('img insert success');
        res.status(200).json({
            status:'success',
            message:'image update success'
        });
    });
}

function getPortrait(req,res,next) {
    console.log('in get img');
    let imgPath = path.join(__dirname, '../img/', req.body.userID+'.jpg');
    // console.log("GET/getImg: ",imgPath);
    // var imgStream = fs.createReadStream(imgPath);//there are some bugs, if the file path is not exited, the whole system will break down

    // imgStream.pipe(res);
    // var imgStream = fs.createReadStream(imgPath)
    var imgStream = fs.createReadStream(imgPath)
        .on('error',function(err){
            console.log('error in get img',err);
            // res.status(404).json({
            //     status:'fail',
            //     message:'no such file'
            // });
            // return;
        })
        .on('complete',function(result){
            console.log('Complete!');
        })
    res.writeHead(200, {"Content-Type": "image/jpeg","Cache-Control":"private"});
    imgStream.pipe(res);
}


function getIdPortrait(req,res,next) {
    // console.log('in get img');
    let imgPath = path.join(__dirname, '../img/', req.params.id+'.jpg');
    console.log("GET/getImg: ",imgPath);
    var imgStream = fs.createReadStream(imgPath)
        .on('error',function(err){
            console.log('error get img id',err);
            // res.status(404).json({
            //     status:'fail',
            //     message:'no such file'
            // });
            // return;
        })
        .on('complete',function(result){
            console.log('Complete!');
        })
    res.writeHead(200, {"Content-Type": "image/jpeg","Cache-Control":"private"});
    imgStream.pipe(res);
}

function writeIdPortrait(req,res,next) {
    req.imgID = req.params.id;
    upload(req,res,function (err) {
        if (err) {
            console.log('Error in multer: ',err);
            return next(err);
        }
        console.log('img insert success');
        res.status(200).json({
            status:'success',
            message:'image update success'
        });
    });
}


module.exports = {
    writePortrait:writePortrait,
    getPortrait:getPortrait,
    getIdPortrait:getIdPortrait,
    writeIdPortrait:writeIdPortrait
}