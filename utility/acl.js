var acl = require('acl');
acl = new acl(new acl.memoryBackend());

/* assign permissions to roles */
acl.allow('STUD', ['studentDetail', 'activity', 'todo'], 'get');
acl.allow('FT', ['studentList','staffList','staffInfo'], 'get');
acl.allow('PMC', ['activity','todo'], 'post');
acl.allow('DOE', 'badge', ['delete','post']);
acl.allow('ADMIN', ['studentInfo', 'user', 'relation'], '*');

acl.addRoleParents('FT', 'STUD');
acl.addRoleParents('PMC', ['STUD','FT']);
acl.addRoleParents('DOE', ['STUD','FT','PMC']);
acl.addRoleParents('ADMIN', ['STUD','FT','PMC','DOE']);

// acl.addUserRoles('vp@me.com', 'ADMIN');
// acl.addUserRoles('ds@me.com', 'STUD');
// acl.addUserRoles('w@me.com', 'PMC');

function addUserRoles(username, role){
    acl.addUserRoles(username, role);
}

function removeUserRoles(username, role){
    acl.removeUserRoles(username, role);
}

function isAuthorized(resource, action){
    /*
     * checkPermission
     * checks if a user has permission to perform an action on a specified resource
     *
     *  @param {string} resource - resource being accessed
     *  @param {string/array} action - action(s) being performed on the resource
     *  @param {object} req - express request object
     *  @param {object} res - express response object
     *  @param {object} next - express middleware next object
     */

    var middleware = false;  // start out assuming this is not a middleware call

    return function(req, res, next){
        // check if this is a middleware call
        if(next){
            // only middleware calls would have the "next" argument
            middleware = true;
        }

        var username = req.body.username;  // get user id property from express request


        // perform permissions check
        acl.isAllowed(username, resource, action, function(err, result){
            // return results in the appropriate way
            if(middleware === true){
                if(result){
                    // user has access rights, proceed to allow access to the route
                    return next();
                } else {
                    // user access denied
                    // todo check username to general name after system complete
                    var checkError = new Error(username + " does not have permission to perform this action on this resource");
                    console.log(checkError);
                    return next(checkError);  // stop access to route
                }
            } else {
                if(result){
                    // user has access rights
                    return true;
                } else {
                    // user access denied
                    return false;
                }
            }
        });
    }
}

module.exports = {
    isAuthorized: isAuthorized,
    addUserRoles: addUserRoles,
    removeUserRoles: removeUserRoles,
};
