var nodemailer = require('nodemailer');
var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
var emailJade = require('./jadeCompiler');

const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../config.yaml'), 'utf8'));

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport(config['EMAILACCOUNT']);

transporter.verify(function(error, success) {
    if (error) {
        console.log(error);
    } else {
        console.log('Server is ready to take our messages');
    }
});

// setup e-mail data with unicode symbols
const mailOptions = {
    from: config['EMAILADDRESS'], // sender address
    to: '', // list of receivers
    subject: '', // Subject line
    text: '', // plaintext body
    html:'',// html body
    attachments: [
    ]
};


function sendMail(input) {
    // standardizeInput(input);
    // console.log(input);
    // console.log('sendMail: ',mailOptions);
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log('send fail: ');
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

function emailHandler(input) {
    if (input.email === 'userCreate') {
        standardizeInput(input);
        // console.log('emailHandler: ',mailOptions);
        emailJade.compile('createUser',input.data,function (err,html) {
            if (err) {
                console.log(err);
            } else {
                mailOptions.html = html;
                sendMail(mailOptions);
            }
        })
    }
}


function standardizeInput(input) {
    for (var i in input) {
        // console.log(i, input[i]);
        // if (!mailOptions.i || mailOptions.i.length == 0) {
            mailOptions[i] = input[i];
        // }
    }
    // console.log('standizeInput',mailOptions);
}

module.exports = {
    sendMail:emailHandler,
}
