var _jade = require('jade');
var fs = require('fs');

exports.compile = function(relativeTemplatePath, data, next){

    // actual path where the template lives on the file system, assumes the standard /views directory
    // output would be something like /var/www/my-website/views/email-template.jade
    var absoluteTemplatePath = process.cwd() + '/email/template/' + relativeTemplatePath + '.jade';

    /*var data = {
        title: 'Test Email Title Goes Here',
        email: req.body.toEmail,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        rightColumnList: ['item #1', 'item #2', 'item #3', 'item #4']
      };
    */
    // var data = {
    //     username: 'wjm'
    // }
    // console.log(data);
    // get our compiled template by passing path and data to jade
    _jade.renderFile(absoluteTemplatePath, data, function(err, compiledTemplate){
        if(err){
            throw new Error('Problem compiling template(double check relative template path): ' + relativeTemplatePath);
        }
        // console.log('[INFO] COMPILED TEMPLATE: ', compiledTemplate)
        next(null, compiledTemplate);
    });
};