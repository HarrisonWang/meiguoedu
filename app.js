var express = require('express');
var path = require('path');
var passport = require('passport');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var auth = require('./routes/auth');
var users = require('./routes/users');
var admin = require('./routes/admin');
var parseRouter = require('./routes/surveyParserRouting');
var api = require('./routes/v1');
var app = express();

var acl = require('./utility/bootacl');
acl.init();

app.enable('trust proxy');

app.use(passport.initialize());
var localSignupStrategy = require('./passport/signup_local_strategy');
passport.use('local-signup', localSignupStrategy);
var localLoginStrategy = require('./passport/login_local_strategy');
passport.use('local-login', localLoginStrategy);
var localPasswordStrategy = require('./passport/password_local_strategy');
passport.use('local-reset-password', localPasswordStrategy);
var jwtCheckStrategy = require('./passport/tokencheck_jwt_strategy');
passport.use('jwt', jwtCheckStrategy);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/static',express.static(path.join(__dirname, 'public/static')));
app.use('/stylesheets',express.static(path.join(__dirname,'public/stylesheets')));
app.use('/images',express.static(path.join(__dirname,'public/images')));
// app.use('/portrait',express.static(path.join(__dirname,'img')));

app.use('/v1', passport.authenticate('jwt', {session: false}), api);
// app.use('')

// app.post('/v1/upload',passport.authenticate('jwt', {session: false}),function (req, res, next) {
//     console.log("POST/upload: ",req);
//     // res.send();
//     // req.files 是 `photos` 文件数组的信息s
//     // req.body 将具有文本域数据, 如果存在的话
//     res.end();
// })
app.use('/', index);
app.use('/auth', auth);
app.use('/parse',parseRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
    // TODO: error handler in the future
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
