var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var pgdb = require('../query');

var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '../config.yaml'), 'utf8'));

var cookieExtractor = function(req) {
    let token = null;
    if (req && req.cookies) {
        token = req.cookies['jwt'];
    }
    return token;
};

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromExtractors([cookieExtractor]);
opts.secretOrKey = config['JWT']['SECRET'];
opts.passReqToCallback = true;

module.exports = new JwtStrategy( opts,
    function(req, jwt_payload, done) {
        // TODO: check cookie expiration is solved by passport-jwt, but need to update token expiration when necessary
        // format of jwt_payload: check login_local_strategy.js
        let unknownUser = {
            body: {
                username: jwt_payload.sub,
                userrole: jwt_payload.role,
            }
        };

        pgdb.getoneFromLoginCB(unknownUser,
            (data) => {
                if(data.role !== unknownUser.body.userrole || data.isvisited === 0){
                    return done(null, false);
                }
                req.body.username = unknownUser.body.username;
                req.body.userrole = unknownUser.body.userrole;
                req.body.userID = data.id;
                return done(null, data);
            },
            (err) => {
                return done(err);
            }
        );
    }
);


