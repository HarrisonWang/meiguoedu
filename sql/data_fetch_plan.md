#STUD
####Summary
* inner join *student table* + *relation table*
* find respective staff bio summary in *staff table*
####TODO
* find tasks in *todo table* using student md5
####Activity
* find activity in *activity table* using student md5

#DOE
####Summary
* inner join *staff table* + *relation table*
* find respective staff(PMC+FT) bio summary in *staff table*
* find respective student bio summary in *student table*
####TODO
* share same info with student todo
####Activity
* share same info with student activity

#PMC
####Summary
* inner join *staff table* + *relation table*
* find respective staff(PMC+FT) bio summary in *staff table*
* find respective student bio summary in *student table*
####TODO
* share same info with student todo
####Activity
* share same info with student activity

#FT
* inner join *staff table* + *relation table*
* find respective staff(DOE+PMC) bio summary in *staff table*
* find respective student bio summary in *student table*
####TODO
* Not applicable
####Activity
* Not applicable

#Admin
* inner join *student table* + *relation table* to get relation
* get all summary from *staff table*