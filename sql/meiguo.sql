

/*
 Navicat Premium Data Transfer

 Source Server         : MeiguoEdu_pg
 Source Server Type    : PostgreSQL
 Source Server Version : 90603
 Source Host           : localhost:5432
 Source Catalog        : meiguoedu
 Source Schema         : meiguo

 Target Server Type    : PostgreSQL
 Target Server Version : 90603
 File Encoding         : 65001

 Date: 09/08/2017 18:24:14
*/


-- ----------------------------
-- Sequence structure for login_isVisited_seq
-- ----------------------------
-- DROP SEQUENCE IF EXISTS "login_isVisited_seq";
-- CREATE SEQUENCE "login_isVisited_seq"
-- INCREMENT 1
-- MINVALUE  1
-- MAXVALUE 9223372036854775807
--  START 1
-- CACHE 1;
-- SELECT setval('"login_isVisited_seq"', 1, false);

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS "activity";
CREATE TABLE "activity" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "type" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "hours" int2 NOT NULL DEFAULT NULL,
  "happi" int2 NOT NULL DEFAULT NULL,
  "detail" jsonb NOT NULL DEFAULT NULL,
  "relation_id" uuid NOT NULL DEFAULT NULL,
  "tier" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "meet_date" date NOT NULL DEFAULT NULL
)
;



-- ----------------------------
-- Table structure for todo
-- ----------------------------
DROP TABLE IF EXISTS "todo";
CREATE TABLE "todo" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "due_date" date NOT NULL DEFAULT NULL,
  "complete" bool NOT NULL DEFAULT NULL,
  "relation_id" uuid NOT NULL DEFAULT NULL,
  "role" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "content" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "recent_modifier" varchar COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL
)
;
-- ALTER TABLE "todo" OWNER TO "wjm-harry";




-- ----------------------------
-- Table structure for relation
-- ----------------------------
DROP TABLE IF EXISTS "relation";
CREATE TABLE "relation" (
  "id" uuid NOT NULL DEFAULT NULL,
  "doe" jsonb DEFAULT NULL,
  "pmc" jsonb DEFAULT NULL,
  "ft" jsonb DEFAULT NULL,
  "stud" jsonb NOT NULL DEFAULT NULL
)
;

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS "staff";
CREATE TABLE "staff" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "bio" jsonb NOT NULL DEFAULT NULL,
  "contact" jsonb NOT NULL DEFAULT NULL,
  "relation_id" uuid[] NOT NULL DEFAULT NULL
)
;
-- ALTER TABLE "staff" OWNER TO "wjm-harry";
-- COMMENT ON COLUMN "staff"."relation_id" IS 'length can be 0';

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS "student";
CREATE TABLE "student" (
  "id" uuid NOT NULL DEFAULT NULL,
  "create_date" date NOT NULL DEFAULT NULL,
  "bio" jsonb NOT NULL DEFAULT NULL,
  "contact" jsonb DEFAULT NULL,
  "hours" integer[] NOT NULL DEFAULT NULL,
  "happi" integer[] NOT NULL DEFAULT NULL,
  "badges" text[] COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "gpa" jsonb DEFAULT NULL,
  "wma" jsonb DEFAULT NULL,
  "wyz" jsonb DEFAULT NULL,
  "vom" jsonb DEFAULT NULL
)
;
-- ALTER TABLE "student" OWNER TO "wjm-harry";

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS "login";
CREATE TABLE "login" (
  "id" uuid NOT NULL DEFAULT NULL,
  "username" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "role" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "password" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "isvisited" int2 NOT NULL DEFAULT 0,
  "nickname" varchar(16) COLLATE "pg_catalog"."default" DEFAULT NULL
)
;
-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
-- ALTER SEQUENCE "login_isVisited_seq"
-- OWNED BY "login"."isvisited";
-- SELECT setval('"login_isVisited_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table activity
-- ----------------------------
ALTER TABLE "activity" ADD CONSTRAINT "activity_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table login
-- ----------------------------
ALTER TABLE "login" ADD CONSTRAINT "login_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table relation
-- ----------------------------
ALTER TABLE "relation" ADD CONSTRAINT "relation_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table staff
-- ----------------------------
ALTER TABLE "staff" ADD CONSTRAINT "staff_contact" UNIQUE ("contact");
COMMENT ON CONSTRAINT "staff_contact" ON "staff" IS 'this jsonb should be unique';

-- ----------------------------
-- Primary Key structure for table staff
-- ----------------------------
ALTER TABLE "staff" ADD CONSTRAINT "staff_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table student
-- ----------------------------
ALTER TABLE "student" ADD CONSTRAINT "contact" UNIQUE ("contact");
COMMENT ON CONSTRAINT "contact" ON "student" IS 'this jsonb should be unique
';

-- ----------------------------
-- Primary Key structure for table student
-- ----------------------------
ALTER TABLE "student" ADD CONSTRAINT "student_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table todo
-- ----------------------------
ALTER TABLE "todo" ADD CONSTRAINT "todo_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table activity
-- ----------------------------
ALTER TABLE "activity" ADD CONSTRAINT "r_id" FOREIGN KEY ("relation_id") REFERENCES "relation" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table relation
-- ----------------------------
ALTER TABLE "relation" ADD CONSTRAINT "relation_doe" FOREIGN KEY ("doe") REFERENCES "staff" ("contact") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "relation" ADD CONSTRAINT "relation_ft" FOREIGN KEY ("ft") REFERENCES "staff" ("contact") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "relation" ADD CONSTRAINT "relation_pmc" FOREIGN KEY ("pmc") REFERENCES "staff" ("contact") ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE "relation" ADD CONSTRAINT "relation_stu" FOREIGN KEY ("stud") REFERENCES "student" ("contact") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table staff
-- ----------------------------
ALTER TABLE "staff" ADD CONSTRAINT "primary" FOREIGN KEY ("id") REFERENCES "login" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table student
-- ----------------------------
ALTER TABLE "student" ADD CONSTRAINT "primary" FOREIGN KEY ("id") REFERENCES "login" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Keys structure for table todo
-- ----------------------------
ALTER TABLE "todo" ADD CONSTRAINT "r_id" FOREIGN KEY ("relation_id") REFERENCES "relation" ("id") ON DELETE CASCADE ON UPDATE RESTRICT;



-- ----------------------------
-- Insert Sample Data
-- ----------------------------
-- ----------------------------
-- Records of login
-- ----------------------------
BEGIN;
INSERT INTO "login" VALUES ('5f7ef216afee0d20af4d5f333c0a9df8', 'wjm@me.com', 'DOE', '$2a$10$VgOu3gW5Z0k1exdQCM1VgOa72Ns0Alig0rS13qoPaNdo0MTcK8fN.', 0, 'wjm');
INSERT INTO "login" VALUES ('4a4c84a9-160d-2c56-4199-8c6ac9b19548', 'vp@me.com', 'ADMIN', '$2a$10$U5PEMDMoGm7.CsQZxDhN6e7m9fmDwbrZx3Splxx5bJOtNuaEXo82m', 22, 'Veronica');
INSERT INTO "login" VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', 'ds@me.com', 'STUD', '$2a$10$9Cuf/Dc6NS0vcZEql3SCqOjv3L1kKHkn.Ue0xMEG4RjrToF.2EDUC', 1, 'Desire');
INSERT INTO "login" VALUES ('46592467-7006-cecf-94ea-05bc11690ab8', 'siyuan@me.com', 'STUD', '$2a$10$VAsXZ1Vaa1tt1fkA/rWA1.IENY.BDBpLpW3F0cXu4xeOrJtJnNwRS', 1, 'c1');
INSERT INTO "login" VALUES ('a2bce774-7224-1256-4354-facb93c3cf83', 'w@me.com', 'PMC', '$2a$10$yfIAFCK6/nUTIVaWYCtU7Oi9Neng01p1QXuZNMPFaWmDfBUzsamD2', 1, 'WJM_PMC');
INSERT INTO "login" VALUES ('ca55a39a-d447-4191-3e08-32e45e79d257', 'j@me.com', 'FT', '$2a$10$5NuXnqfOPJHiai46zS2DU.OkynMJ3O2MIvAuPI8GEecsDnJeXJnEq', 1, 'WJM_FT');
INSERT INTO "login" VALUES ('45b108f2-301e-5076-9d63-5a18c248a6b3', 'harrison@meiguola.com', 'STUD', '$2a$10$BWckqRqFUXqmJol9RbPlXOPIBMdpk/bJp.rjkjjK4sOFPVe6Ip05u', 0, '');
COMMIT;

INSERT INTO "staff" VALUES (
  '4a4c84a9-160d-2c56-4199-8c6ac9b19548','2017-07-27',
  '{"summary_name":"veronica prout","summary_preferred_name":"veronica","summary_gender":"Female","summary_birthday":"1990-05-09","summary_religion":"none","role":"ADMIN","photo":"./img/4a8a08f09d37b73795649038408b5f33.png"}',
  '{"id":"4a4c84a9-160d-2c56-4199-8c6ac9b19548","summary_email":"veronica@meiguola.com","summary_phone":"324432432423","summary_wechat":"jinxiangcao","summary_skype":"veronica","summary_name":"veronica prout"}','{}');

INSERT INTO "staff" VALUES (
  '5f7ef216afee0d20af4d5f333c0a9df8','2017-07-27',
  '{"summary_name":"wang jiameng","summary_preferred_name":"harrison","summary_gender":"Male","summary_birthday":"1993-07-01","summary_religion":"none","role":"DOE","photo":"./img/4a8a08f09d37b73795649038408b5f33.png"}',
  '{"id":"5f7ef216afee0d20af4d5f333c0a9df8","summary_email":"wjm@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_skype":"fdsads","summary_name":"jiameng wang"}','{ffaff937-53e0-bfb5-0b56-aa0d83d9b1be}');

INSERT INTO "staff" VALUES (
  'a2bce774-7224-1256-4354-facb93c3cf83','2017-07-27',
  '{"summary_name":"PMC1 jiameng","summary_preferred_name":"harrison","summary_gender":"Male","summary_birthday":"1993-07-01","summary_religion":"none","role":"PMC","photo":"./img/4a8a08f09d37b73795649038408b5f33.png"}',
  '{"id":"a2bce774-7224-1256-4354-facb93c3cf83","summary_email":"w@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_skype":"wjm_pmc","summary_name":"PMC1 jiameng"}','{ffaff937-53e0-bfb5-0b56-aa0d83d9b1be}');

INSERT INTO "staff" VALUES (
  'ca55a39a-d447-4191-3e08-32e45e79d257','2017-07-27',
  '{"summary_name":"FT1 Wang","summary_preferred_name":"harrison","summary_gender":"Male","summary_birthday":"1993-07-01","summary_religion":"none","role":"FT","photo":"./img/4a8a08f09d37b73795649038408b5f33.png"}',
  '{"id":"ca55a39a-d447-4191-3e08-32e45e79d257","summary_email":"j@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_skype":"fdsads","summary_name":"FT1 Wang"}','{ffaff937-53e0-bfb5-0b56-aa0d83d9b1be}');


INSERT INTO "student" VALUES(
  'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be',
  '2017-07-27',
  '{"summary_name":"desire Sayarath","summary_skype": "desiredesire", "summary_gender": "Female", "summary_birthday": "1/1/1993","summary_native_name": "茉莉", "summary_preferred_name": "desire", "family_bio_siblings": "No", "school_chinese_name": "desire","summary_religion":"none","summary_street":"830 Traction Ave","summary_city":"Los Angeles","summary_district":"CA","summary_zipcode":"90013","country":"USA","street":"830 Traction Ave","city":"Los Angeles","district":"CA","zipcode":"90013","country":"USA","home_street":"830 Traction Ave","home_city":"Los Angeles","home_state":"CA","home_zipcode":"90013","home_country":"USA","primary_language":"english","school_english_name": "wjm", "extracurricular_work": "wjm", "school_chinese_pinyin": "desire", "extracurricular_volunteer": "desire", "extracurricular_education_reason": "Get a job"}',
  '{"id":"ffaff937-53e0-bfb5-0b56-aa0d83d9b1be","summary_email":"ds@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_name":"desire Sayarath","summary_skype": "desiredesire"}',
  '{1,2,3,8,0}',
  '{2,3,4,6,8}',
  '{"b1","c4"}',
  '{"GPA_website":"whwh","GPA_username":"hdgfjh","GPA_password":"hjkh"}',
  '{"WMA_day":"","WMA_time":"","WMA_location":""}',
  '{"WyZ_username":"","WyZ_password":""}',
  '{"VoM_username":"","VoM_password":""}'
);

INSERT INTO "student" VALUES (
  '45b108f2-301e-5076-9d63-5a18c248a6b3',
  '2017-11-01',
  '{"fb_2": "Yes", "ah_AP": "No", "ah_type": "Day", "pi_work": "fjhsajfhdjshfjhjkfhkj", "ah_grade": "1styearMaster''s", "ah_tests": "TOEFL", "pi_clubs": "Nonetoshare", "pi_drive": "Yes", "ah_grades": "MostlyA''sandB''s", "fb_1_name": "Hong Wang", "fb_2_city": "徐州市", "fb_2_name": "mother", "pi_admire": "Steve Jobs", "pi_awards": "Nonetoshare", "pi_leader": "jhfdsjkghfkjdhj", "pi_sports": "Nonetoshare", "pi_summer": "Sleeping", "pi_traits": ["Knowledge", "Patience", "Wisdom", "Stability", "Curiosity"], "fb_1_email": "jiameng@usc.edu", "fb_1_phone": "2132967283", "fb_1_skype": "none", "fb_2_email": "jwang37@nyit.edu", "fb_2_phone": "2132967823", "fb_2_skype": "none", "fb_2_state": "江苏", "fb_1_degree": "Master''sDegree", "fb_1_wechat": "wjmParent", "fb_2_degree": "Master''sDegree", "fb_2_street": "中山北路227号", "fb_2_wechat": "Mwechat", "fb_guardian": "Yes", "pi_anything": "hdfjsahfdqyg  gfyagfsdfewbqjb , hgkdhsgaf f fdsfgethbhgfbhrtbhrfghrtuhy", "ah_strengths": "focus", "ah_withdrawn": "No", "fb_2_country": "中国", "fb_2_zipcode": "221000", "fb_sibling_1": "Yes", "pi_strengths": "dasfdsfdsf", "pi_volunteer": "hdsfkhdjakfjksdahf", "summary_city": "Los Angeles", "summary_name": "Jiameng Wang", "ah_skip_grade": "No", "ah_weaknesses": "none", "fb_1_birthday": "08/25/1966", "fb_2_birthday": "09/25/1967", "fb_2_district": "鼓楼区", "fb_2_relation": "Mother", "fb_guardian_2": "No", "pi_weaknesses": "fdsafdaf", "summary_email": "harrison@meiguola.com", "summary_phone": "2132967823", "summary_skype": "wjm_harry@me.com", "summary_state": "CA", "ah_AP_interest": ["ComputerScienceA", "ComputerSciencePrinciples", "ChineseLanguageandCulture"], "ah_disruptions": "Nothing to tell", "ah_school_name": "USC", "fb_1_birthcity": "XUZHOU", "fb_2_birthcity": "xuzhou", "summary_gender": "Male", "summary_street": "2700 Ellendale Pl", "summary_wechat": "Wjm2011W", "ah_disciplinary": "No", "ah_repeat_grade": "No", "fb_1_occupation": "Policeman", "fb_2_occupation": "teacher", "pi_art_interest": ["Choir/Choral/Singing", "GraphicDesign", "JazzBand"], "pi_difficulties": "jfdhajfhdjkshf fdgsha f  qgdsafg ds f sghfdsjds fjdha fawhuigdsfds.gfs.fgfd,gfdsg", "summary_zipcode": "90007", "ah_grades_recent": "3.67", "fb_1_citizenship": "CHINA", "fb_1_native_name": "王洪", "fb_2_citizenship": "China", "fb_2_native_name": "wang mother", "fb_sibling_total": "4", "pi_peace_freedom": "peace", "summary_birthday": "07/01/1993", "summary_district": "None", "summary_out_city": "Xuzhou", "summary_religion": "NoReligion", "ah_college_course": "Introduction to programming language", "fb_1_company_city": "徐州市", "fb_1_relationship": "Father", "fb_2_company_city": "徐州市", "fb_2_company_name": "mother company", "fb_sibling_1_name": "cousinOne", "fb_sibling_2_name": "cousinTwo", "fb_sibling_3_name": "cousinThree", "summary_birthcity": "Xu Zhou,Jiang Su, China", "summary_out_state": "JiangSu", "ah_grade_completed": "Master''s", "ah_previous_attend": "Yes", "fb_1_company_phone": "2379874367", "fb_1_company_state": "江苏", "fb_1_recent_school": "parent recent school", "fb_2_company_phone": "133338976655", "fb_2_company_state": "江苏省", "fb_2_recent_school": "mother college", "pi_sports_interest": ["Golf", "Gymnastics", "Lacrosse"], "pi_volunteer_cause": ["Environment", "Health&Medicine", "Immigrants&Refugees"], "summary_out_street": "north zhongshan rd 227", "fb_1_preferred_name": "王洪", "fb_2_company_street": "中山北路227号", "fb_2_preferred_name": "母亲", "fb_guardian_1_email": "local@me.com", "fb_guardian_1_phone": "12332141223", "fb_guardian_1_skype": "none", "fb_sibling_1_gender": "Male", "fb_sibling_2_gender": "Male", "fb_sibling_3_gender": "Male", "summary_citizenship": "China", "summary_living_with": ["Father", "Mother"], "summary_native_name": "王佳梦", "summary_out_zipcode": "221000", "ah_favourite_subject": ["Calculus", "EnvironmentalScience", "Music", "Photography"], "fb_1_company_country": "中国", "fb_1_company_zipcode": "221000", "fb_1_relation_status": "Married", "fb_2_company_country": "中国", "fb_2_company_zipcode": "221000", "fb_2_relation_status": "Married", "fb_guardian_1_wechat": "localguardianWehchat", "summary_out_district": "Gu Lou", "ah_school_native_name": "南加州大学", "fb_1_company_district": "鼓楼区", "fb_2_company_district": "鼓楼区", "fb_guardian_1_company": "MeiguoEdu", "fb_guardian_1_english": "Local Gardian", "fb_sibling_1_birthday": "08/02/1993", "fb_sibling_2_birthday": "03/20/1997", "fb_sibling_3_birthday": "09/21/2000", "pi_assistance_classes": "No", "pi_coursework_outside": "fjdhsgjfhdsjgkh", "fb_guardian_1_birthday": "09/10/1995", "fb_guardian_1_relation": "parent''s friend", "pi_dream_school_career": "USC", "summary_preferred_name": "Harrison", "ah_learning_dificulties": "No", "ah_previous_school_name": "NYIT", "ah_previous_school_type": "Day", "fb_1_recent_school_year": "1988", "fb_2_recent_school_year": "1989", "fb_sibling_1_schoolname": "USC", "fb_sibling_1_schoolyear": "2017", "fb_sibling_2_schoolname": "USC", "fb_sibling_2_schoolyear": "2016", "fb_sibling_3_schoolname": "High School", "fb_sibling_3_schoolyear": "2016", "pi_orgnization_interest": ["ComputerGamingClub", "ComputerGraphicsClub", "DebateClub", "EngineeringClub"], "fb_1_company_native_name": "徐州交巡警支队", "fb_2_company_native_name": "母亲的公司", "fb_guardian_1_occupation": "web developer", "fb_sibling_1_native_name": "兄弟1号", "fb_sibling_2_native_name": "兄弟2号", "fb_sibling_3_native_name": "兄弟3号", "summary_primary_language": "Chinese", "fb_1_company_english_name": "XuZhouJiaoXunJingZhiDui", "ah_previous_school_end_date": "05/12/2015", "fb_1_company_company_street": "中山北路227", "fb_1_recent_school_interest": "pilot", "fb_2_recent_school_interest": "Microwave", "fb_sibling_1_educationlevel": "Master''sDegree", "fb_sibling_1_preferred_name": "cousinOne", "fb_sibling_1_schoolinterest": "CS", "fb_sibling_2_educationlevel": "Bachelor''sDegree", "fb_sibling_2_preferred_name": "cousinTwo", "fb_sibling_2_schoolinterest": "ECE", "fb_sibling_3_educationlevel": "HighSchoolEducation/Diploma", "fb_sibling_3_preferred_name": "cousinThree", "fb_sibling_3_schoolinterest": "none", "fb_guardian_1_driver_license": "EGHJH313K", "fb_guardian_1_preferred_name": "gar", "ah_previous_school_start_date": "09/01/2011", "fb_guardian_1_relation_status": "bacholar", "ah_previous_school_native_name": "纽约理工", "fb_1_recent_school_native_name": "最近学校", "fb_2_recent_school_native_name": "母亲的大学", "fb_sibling_1_native_schoolname": "南加州大学", "fb_sibling_2_native_schoolname": "南加州大学", "fb_sibling_3_native_schoolname": "高中", "fb_guardian_1_company_native_name": "美国教育", "ah_previous_school_grade_completed": ["1styearCollege/University", "2ndyearCollege/University", "3rdyearCollege/University", "4thyearCollege/University"]}',
  '{"id": "45b108f2-301e-5076-9d63-5a18c248a6b3", "summary_name": "Jiameng Wang", "summary_email": "harrison@meiguola.com", "summary_phone": "2132967823", "summary_skype": "wjm_harry@me.com", "summary_wechat": "Wjm2011W"}',
  '{0,0,0,0,0}',
  '{0,0,0,0,0}',
  '{c1}',
  '{"GPA_website": "", "GPA_password": "", "GPA_username": ""}',
  '{"WMA_day": "", "WMA_time": "", "WMA_location": ""}',
  '{"WyZ_password": "", "WyZ_username": ""}',
  '{"VoM_password": "", "VoM_username": ""}'
);



INSERT INTO "relation" ("id", "doe", "pmc", "ft", "stud") VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', '{"id":"5f7ef216afee0d20af4d5f333c0a9df8","summary_email":"wjm@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_skype":"fdsads","summary_name":"jiameng wang"}', '{"id":"a2bce774-7224-1256-4354-facb93c3cf83","summary_email":"w@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_skype":"wjm_pmc","summary_name":"PMC1 jiameng"}', '{"id":"ca55a39a-d447-4191-3e08-32e45e79d257","summary_email":"j@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_skype":"fdsads","summary_name":"FT1 Wang"}','{"id":"ffaff937-53e0-bfb5-0b56-aa0d83d9b1be","summary_email":"ds@me.com","summary_phone":"2132145434","summary_wechat":"meiguola","summary_name":"desire Sayarath","summary_skype": "desiredesire"}');

INSERT INTO "relation" ("id", "doe", "pmc", "ft", "stud") VALUES ('45b108f2-301e-5076-9d63-5a18c248a6b3', NULL, NULL, NULL, '{"id": "45b108f2-301e-5076-9d63-5a18c248a6b3", "summary_name": "Jiameng Wang", "summary_email": "harrison@meiguola.com", "summary_phone": "2132967823", "summary_skype": "wjm_harry@me.com", "summary_wechat": "Wjm2011W"}');


INSERT INTO "activity" VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', '2017-07-01', 'SKILL', 2, 5, '{"name": "dsf", "describe": "fdgfdsg", "position": "afds"}', 'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', '1', '2017-09-01');
INSERT INTO "activity" VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d8b1be', '2017-08-01', 'WORK', 0, 1, '{"name": "adfsd", "describe": "dsaf", "position": "dafds"}', 'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', '1', '2017-07-01');
INSERT INTO "activity" VALUES ('7296b6ca-1fa2-4193-954c-ab47e9018a7c', '2017-11-01', 'Internship', 2, 4, '{"name": "first act", "describe": "this is great!", "position": "leader"}', '45b108f2-301e-5076-9d63-5a18c248a6b3', '2', '2017-10-31');
INSERT INTO "activity" VALUES ('07d16265-e5ba-463a-a541-85e899cb4b46', '2017-11-01', 'Computer/Technology', 5, 2, '{"name": "second act", "describe": "waiting for command from leader", "position": "teammember"}', '45b108f2-301e-5076-9d63-5a18c248a6b3', '4', '2017-10-17');

INSERT INTO "todo" VALUES ('ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', '2017-07-11', '2017-07-11', 't', 'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', 'STUD', 'dsfsdfdsfdsf', 'ADMIN');
INSERT INTO "todo" VALUES ('ffaff937-53e0-bfb5-0b56-aa0d88d9b2be', '2017-07-01', '2017-07-11', 't', 'ffaff937-53e0-bfb5-0b56-aa0d83d9b1be', 'PMC', 'this is todo for pmc', 'ADMIN');
INSERT INTO "todo" VALUES ('de1cdbee-2026-419b-831f-a48f4a99362b', '2017-11-01', '2017-11-10', 'f', '45b108f2-301e-5076-9d63-5a18c248a6b3', 'FT', 'first todo for FT， 你需要translate', 'ADMIN');
INSERT INTO "todo" VALUES ('31eba2e0-c567-42dd-a642-6c3bde178420', '2017-11-01', '2017-11-23', 'f', '45b108f2-301e-5076-9d63-5a18c248a6b3', 'STUD', 'second todo for student, what out boy!', 'ADMIN');
INSERT INTO "todo" VALUES ('7cb3e4a8-02a1-4315-8133-c243d48e7669', '2017-11-01', '2017-11-23', 'f', '45b108f2-301e-5076-9d63-5a18c248a6b3', 'PMC', 'first todo for PMC, don''t forget meeting this week', 'ADMIN');
INSERT INTO "todo" VALUES ('d9fdcc7f-af21-4aab-b47c-5aa02626712d', '2017-11-01', '2017-11-23', 'f', '45b108f2-301e-5076-9d63-5a18c248a6b3', 'PMC', 'second todo for PMC, talk with DOE', 'ADMIN');
INSERT INTO "todo" VALUES ('e587587b-e1c4-4ec2-91a7-80a0be89c91f', '2017-11-01', '2017-11-10', 't', '45b108f2-301e-5076-9d63-5a18c248a6b3', 'STUD', 'first todo， 你需要test更多功能', 'ADMIN');
