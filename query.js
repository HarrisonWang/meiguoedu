var yaml = require('js-yaml');
var fs = require('fs');
var path = require('path');
const config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, './config.yaml'), 'utf8'));

var mail = require('./email/mail');
var promise = require('bluebird');
var options = { promiseLib: promise };
var validator = require('validator');
var pgp = require('pg-promise')(options);
var db = pgp(config['POSTGRE']['URI']);

var acl = require('./utility/acl');

const uuidv4 = require('uuid/v4');
const uuidv5 = require('uuid/v5');
const UUIDSEED = config['UUID']['NAMESPACE'];

const ROLES = ['STUD','FT','DOE','PMC'];


db.proc('version')
    .then(data => {
        // SUCCESS
        // data.version =
        // 'PostgreSQL 9.5.1, compiled by Visual C++ build 1800, 64-bit'
        console.log('DB version: ',data)
    })
    .catch(error => {
        console.log('DB connection refused: ',error)
    });


/*
user {
  id: '03e5d82b-05d9-df15-c717-24d39ea4c6',
  username: 'example@example.com',
  role: 'STUD',
  password: '$2a$10$zR9DjxZ39fecG4a7275jtvCMzgnVVO0m4R1cRr3b7styana',
  isvisited: 0,
  nickname: ''
}
*/

module.exports = {
    getAllLoginCB:getAllLoginCB,
    getoneFromLoginCB:getoneFromLoginCB,
    getstudentSummary: getstudentSummary,
    getStudentDetail:getStudentDetail,
    getAllStaffs: getallStaffs,
    getStaffDetail:getStaffDetail,

    createOneUserCB:createOneUserCB,
    createUser:createOneUser,
    createStudent:createStudent,
    createStaff:createStaff,

    updateStudent:updateStudent,

    relationHandler:relationHandler,

    getTodo: getTodo,
    logTodo: logTodo,
    deleteTodo:deleteTodo,
    updateTodo:updateTodo,

    removeUser:removeOneUser,
    removeOneBadge:removeOneBadge,
    addOneBadge:addOneBadge,

    userFirstLoginResetCB: userFirstLoginResetCB,
    updateIsVistedCB: updateIsVistedCB,

    getActivity:getActivity,
    logActivity: logActivity,

    isAllInfoReadyCB: isAllInfoReadyCB,

    // fetchrole : fetchRole,
    // getoneFromLogin:getoneFromLogin,
    // getOneActivityRecord:getoneActivityRecord,
    // getAllActivity:getAllActivity,
    // getOneStudent: getoneStudent,
    // getOneStaff: getoneStaff,
};

function getAllLoginCB(thenCallBack, catchCallBack) {
    db.any('SELECT username, role FROM login')
        .then(function (data) { thenCallBack(data);})
        .catch(function (err) { catchCallBack(err);});
}

function getoneFromLoginCB(req, thenCallBack, catchCallBack) {
    var username = req.body.username;
    db.one('select * from login where username = $1',username)
        .then(function(data) { thenCallBack(data); })
        .catch(function (err) { catchCallBack(err); })
}

function getstudentSummary(req, res, next){
    let cmd = '';
    if(!req.body.userrole || !req.body.userID){
        let err = new Error("request does not have enough user information");
        console.log(err);
        return next();
    }
    if(req.body.userrole === 'ADMIN'){
        cmd = 'SELECT s.id, s.bio, r.doe, r.pmc, r.ft FROM student s JOIN relation r ON r.id = s.id;';
        db.any(cmd).then(function (data) {
            /* chinese name
            *  chinese name in pinyin
            *  english name
            *  gender
            *  hometown
            *  PMC
            *  DOE
            *  FT */
            let entries = data.map(function(entry){
                console.log(entry.bio);
                return {
                    STUDID: entry.id,
                    NN: entry.bio.summary_native_name,
                    EN: entry.bio.summary_name,
                    gender: entry.bio.summary_gender,
                    hometown: entry.bio.summary_birthcity,
                    PMC: !entry.pmc ? 'Not Designated' : !entry.pmc.name ? 'NA' : entry.pmc.name,
                    DOE: !entry.doe ? 'Not Designated' : !entry.doe.name ? 'NA' : entry.doe.name,
                    FT: !entry.ft ? 'Not Designated' : !entry.ft.name ? 'NA' : entry.ft.name,
                };
            });
            // console.log(entries);
            return res.status(200).json(entries);
        }).catch(function (err) {
            console.log(err);
            return next(err);
        });
    }
    else {
        let cmd = 'SELECT s.id, s.bio, r.doe, r.pmc, r.ft FROM student s ' +
                    'JOIN relation r ON r.id = s.id ' +
                    'AND r.id IN (' +
                        'SELECT unnest(staff.relation_id) FROM staff ' +
                        `WHERE staff.id = '${req.body.userID}' );`;
        db.any(cmd).then(function (data) {
            let entries = data.map(function(entry){
                return {
                    STUDID: entry.id,
                    CN: entry.bio.chinese_name,
                    CNPY: entry.bio.given_name + ' ' + entry.bio.family_name,
                    EN: entry.bio.english_name,
                    gender: entry.bio.gender,
                    hometown: entry.bio.hometown,
                    PMC: !entry.pmc ? 'Not Designated' : !entry.pmc.name ? 'NA' : entry.pmc.name,
                    DOE: !entry.doe ? 'Not Designated' : !entry.doe.name ? 'NA' : entry.doe.name,
                    FT: !entry.ft ? 'Not Designated' : !entry.ft.name ? 'NA' : entry.ft.name,
                };
            });
            // console.log(entries);
            return res.status(200).json(entries);
        }).catch(function (err) {
            console.log(err);
            return next(err);
        });
    }
}

function getStudentDetail(req,res,next) {
    if(!req.body.userrole || !req.body.userID){
        let err = new Error("request does not have enough user information");
        console.log(err);
        return next();
    }

    if(validator.isUUID(req.params.studentinfo)){
        req.params.id = req.params.studentinfo;
        if(req.body.userrole === 'ADMIN'){
            return fetchParseStudentDetail(req, res, next);
        }
        if(req.body.userrole === 'STUD' && req.body.userID === req.params.id){
            return fetchParseStudentDetail(req, res, next);
        }
        if(['FT','DOE','PMC'].indexOf(req.body.userrole) !== -1){
            // check req.params.id in their relation_id array
            // continue if in it
            let cmd = 'SELECT * FROM staff ' +
                `WHERE staff.id = '${req.body.userID}' AND '${req.params.id}' = ANY(relation_id);`;
            let studID = req.params.id;
            return db.any(cmd).then( function(data) {
                if(data.length === 0){
                    let err = new Error("request student not related with user");
                    console.log(err);
                    return next();
                }
                req.params.id = studID;
                return fetchParseStudentDetail(req,res,next);
            }).catch(function (err) {
                console.log(err);
                return next();
            });
        }
        return next();
    }
    else {
        db.one('SELECT id FROM login WHERE username = $1',req.params.studentinfo)
            .then(data => {
                req.params.id = data.id;
                if(req.body.userrole === 'ADMIN'){
                    return fetchParseStudentDetail(req, res, next);
                }
                if(req.body.userrole === 'STUD' && req.body.userID === req.params.id){
                    return fetchParseStudentDetail(req, res, next);
                }
                if(['FT','DOE','PMC'].indexOf(req.body.userrole) !== -1){
                    // check req.params.id in their relation_id array
                    // continue if in it
                    let cmd = 'SELECT * FROM staff ' +
                        `WHERE staff.id = '${req.body.userID}' AND '${req.params.id}' = ANY(relation_id);`;
                    let studID = req.params.id;
                    return db.any(cmd).then( function(data) {
                        if(data.length === 0){
                            let err = new Error("request student not related with user");
                            console.log(err);
                            return next();
                        }
                        req.params.id = studID;
                        return fetchParseStudentDetail(req,res,next);
                    }).catch(function (err) {
                        console.log(err);
                        return next();
                    });
                }
                return next();
            })
            .catch(err => {
                console.log(err);
                return next();
            });
    }

}

function fetchParseStudentDetail(req,res,next) {
    /**this function retrieve all data for student detail page
     * notice: out is the final result, 闭包（closure）feature is used, decrease performance???????
     * */
    var out = {};
    db.task(t => {
        return t.one('select * from student where id = $1', req.params.id)
            .then(user => {
                out['stud'] = user;
                return t.one('select * from relation where id = $1',user.id)
                    .then(relation => {
                        out['relation'] = relation;
                        return t.batch([
                            t.any('select * from todo where relation_id = $1', relation.id),
                            t.any('select * from activity where relation_id = $1', relation.id)
                        ]).then(data => {
                            out['activity'] = data[1];
                            out['todo'] = data[0];
                        });
                    });
            });
    }).then(function (events) {
        out.todo = filterTodo(out.todo);
        res.json(out);
    }).catch(function (err) {
        // console.log(err);
        return next(err);
    });
}

function getallStaffs(req,res,next) {
    db.any('select * from staff')
        .then(function (data) {
            res.status(200).json({
                status:'success',
                data:data,
                message:'retrieve All staffInfo'
            });
        }).catch(function (err) {
        return next(err);
    });
}

function getStaffDetail(req,res,next) {
    var out = {};
    db.task(t => {
        return t.one('select * from staff where id = $1',req.params.id)
            .then(staff =>{
                out['staff'] = staff;
                for (var i in staff.relation_id) {
                    return t.any('select * from todo where relation_id = $1',staff.relation_id[i])
                        .then(todolist => {
                            if (!out.todo) {
                                out.todo = [];
                            }
                            out.todo.push(todolist);
                        });
                }
            });
    }).then(function (events) {
        res.json(out);
    }).catch(function (err) {
        return next(err);
    })
}

function createOneUserCB(req,thenCallBack,catchCallBack) {
    var data = standardizeIn(req.body,'newuser');
    if(data instanceof Error){
        return res.status(400).json({
            status: 'fail',
            message: 'newuser format error'
        });
    }
    db.none('insert into login (id,username,role,password,isvisited,nickname)'+
        'values(${id},${username},${role},${password},${isvisited},${nickname})',
        data)
        .then(function () {
            acl.addUserRoles(data.username, data.role);
            var input = {
                to:data.username,
                subject:'welcome new user',
                data:{
                    username:req.body.username,
                    password:req.body.origPassword
                },
                email:'userCreate'
            }
            mail.sendMail(input);
            thenCallBack();
        })
        .catch(function (err) {
            catchCallBack(err);
    });
}

function createOneUser(req,res,next) {
    var data = standardizeIn(req.body,'newuser');
    if(data instanceof Error){
        return res.status(400).json({
            status: 'fail',
            message: 'newuser format error'
        });
    }
    db.none('insert into login (id,username,role,password,isvisited,nickname)'+
        'values(${id},${username},${role},${password},${isvisited},${nickname})',
        data)
        .then(function () {
            // we need to send email at here
            acl.addUserRoles(data.username, data.role);
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Inserted one user'
                });
        }).catch(function (err) {
            console.log(err);
            return next(err);
        });
}

function createStudent(req,res,next) {
    /**behaviour: student biographical infotmation should existed in req.body
     * method: POST
     * note: insert record into student/relation tables
     *       this method will be called by survery parser when we receive input data
     * */
    // console.log(req.body);
    var parseData = standardizeIn(req.body,'student');
    // console.log(parseData);
    db.none('insert into student (id,create_date,bio,contact,hours,happi,badges,gpa,wma,wyz,vom)'+
        'values (${id},${create_date},${bio},${contact},${hours},${happi},${badges},${gpa},${wma},${wyz},${vom})',parseData
    ).then(function () {
        db.none('insert into relation (id,stud) values' +
                '(${id},${stud})',{'id':parseData.id,'stud':parseData.contact}
        ).then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Inserted one Student Bio'
                });
        }).catch(function (err) {
            console.log(err);
            return next(err);
        })
    }).catch(function (err) {
        console.log(err);
        return next(err);
    })
}

function createStaff(req,res,next) {
    /**behaviour: staff biographical information shoud existed in req.body
     * method: POST
     * note: only insert into staff table
     *       this method will be called by survery parser when we receive input data
     * */
    // console.log(req.body);
    db.none('insert into staff (id,create_date,bio,contact,relation_id,role)'+
        'values (${id},${create_date},${bio},${contact},${relation_id},${role})',standardizeIn(req.body,'staff')
    ).then(function () {
        res.status(200)
            .json({
                status: 'success',
                message: 'Insert one Staff Bio'
            });
    }).catch(function (err) {
        console.log(err);
        return next(err);
    })
}

function updateStudent(req,res,next) {
    const StudentInfoCanBeUpdated = Object.freeze(
        ['bio','contact','hours','happi','badges','gpa','wma','wyz','vom']
    );
    const ArrayFields = Object.freeze(['hours','happi','badges']);
    if(!req.body.target || !req.body.data) {
        return res.status(400).json({
            status: 'fail',
            message: 'target or data fields is missing',
        });
    }
    if(StudentInfoCanBeUpdated.indexOf(req.body.target) === -1){
        return res.status(403).json({
            status: 'fail',
            message: 'request update field can not be modified',
        });
    }
    if(ArrayFields.indexOf(req.body.target) !== -1){
        if(!Array.isArray(req.body.data)){
            return res.status(403).json({
                status: 'fail',
                message: `${req.body.target} suppose be array data type`,
            });
        }
    }
    let payload = {
        target: req.body.target,
        data: req.body.data,
        id: req.params.id,
    };
    db.none('UPDATE student SET ${target^} = ${data} WHERE id = ${id}', payload)
        .then(() => {
            return res.status(200).json({ status: 'success'});
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json({
                status: 'fail',
                message: err,
            });
        });
}

function relationHandler(req,res,next) {
    if (req.body.action) {
        if (req.body.action === "bind") {
            bindRelation(req,res,next);
        } else if(req.body.action === "unbind") {
            unbindRelation(req,res,next);
        } else {
            res.status(200)
                .json({
                    status: 'fail',
                    message: 'Can\'t recognize input value'
                });
        }
    } else {
        res.status(200)
            .json({
                status: 'fail',
                message: 'Wrong input format'
            });
    }
}

function bindRelation(req,res,next) {
    /** bind PMC/FT/DOE into student's relation record
     * i: { staffEmail, studentId }
     * o: success or not
     * */

    db.task(t =>{
        let queries = [];
        queries.push(t.any('SELECT * FROM relation WHERE id = \'${studentId^}\'', req.body));
        queries.push(t.any('SELECT s.id AS staff_id, s.relation_id, lower(l.role) AS staff_role FROM staff s ' +
                            'JOIN login l ON s.id = l.id ' +
                            'WHERE l.username = ${staffEmail};', req.body));
        return t.batch(queries).then(result => {
            /* batch.result = [ [ anonymous {relation entry} ], [ anonymous {staff entry} ] ]
            * relation entry: to prevent unknown studentId insert to staff.relation_id
            * staff entry: to get staff id and role and relation_id */
            // console.log(result);
            if(result[0].length===0) return;
            if(result[1].length===0) return {err: 'wrong staff email'};



            result = result[1][0];
            // console.log(result);
            req.body['staffRole'] = result.staff_role;
            req.body['staffId'] = result.staff_id;

            if(result.relation_id.indexOf(req.body.studentId) === -1){
                //staff haven't bind with this student
                return t.one('UPDATE staff SET relation_id = relation_id || \'{${studentId^}}\' ' +
                    'WHERE id = ${staffId} returning contact', req.body)
                    .then(result => {
                        // console.log(result);
                        req.body['staffContact'] = JSON.stringify(result.contact);
                        return t.none('UPDATE relation SET ${staffRole^} = ${staffContact} ' +
                                      'WHERE id = ${studentId}', req.body).then(() => {
                            return {succ: req.body['staffContact']};
                        });
                    });
            }
        });

    }).then(function (message) {
        // console.log(message);
        res.json({
            status: message.err ? 'fail' : 'success',
            message: message.err ? message.err : message.succ,
        });
    }).catch(function (err) {
        // console.log(err);
        res.status(400).json({
            status: 'fail',
            message: err
        });
    });
}

function unbindRelation(req,res,next) {
    /** unbind PMC/FT/DOE into student's relation record
     * i: req.body: {staffEmail, studentId}
     * o: success or not
     * */

    db.task(t=>{
        let queries = [];
        queries.push(t.any('SELECT * FROM relation WHERE id = \'${studentId^}\'', req.body));
        queries.push(t.any('SELECT s.id AS staff_id, s.relation_id, lower(l.role) AS staff_role FROM staff s ' +
                            'JOIN login l ON s.id = l.id ' +
                            'WHERE l.username = ${staffEmail};', req.body));
        return t.batch(queries).then(result => {
            // no relation find to unbind
            if(result[0].length===0) return;
            // no staff find to unbind
            if(result[1].length===0) return;

            let relationResult = result[0][0];
            let staffResult = result[1][0];
            req.body['staffRole'] = staffResult.staff_role;
            req.body['staffId'] = staffResult.staff_id;

            // if staff in DB have not bind request student,
            if(staffResult.relation_id.indexOf(req.body.studentId) === -1) return;
            // if student in DB do not bind request staff
            // TODO: commend back after system db have changed md5 to uuid
            // if(relationResult[req.body.staffRole] === null ||
            //     relationResult[req.body.staffRole].id !== req.body['staffId']) return;
            queries = [
                t.none('UPDATE staff SET relation_id = array_remove(relation_id,${studentId}) ' +
                    'where id = ${staffId}',req.body),
                t.none('UPDATE relation SET ${staffRole^} = null WHERE id = ${studentId}',req.body)];
            return t.batch(queries).then(function () {return});
        });
    }).then(function (message) {
        res.json({
            status: message ? 'fail' : 'success',
            message: message ? message : null,
        });
    }).catch(function (err) {
        res.status(400).json({
            status: 'fail',
            message: err
        });
    })
}

function getTodo(req,res,next){
    return db.any('SELECT * FROM todo WHERE relation_id = ${id}', req.params)
        .then(data => {
            // console.log(data);
            let todos = {STUD: [], STAFF: []};
            for(let i=0; i<data.length; i++){
                delete data[i].id;
                delete data[i].relation_id;
                data[i].role === 'student' ? todos.STUD.push(data[i]) : todos.STAFF.push(data[i]);
            }
            return res.status(200).json({
                status: 'success',
                message: todos,
            });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json({
                status: 'fail',
                message: err,
            });
        });
}

function logTodo(req,res,next){
    var data = standardizeIn(req.body, 'todo');
    if(data instanceof Error){
        return res.status(400).json({
            status: 'fail',
            message: 'todo format error'
        });
    }
    db.none('INSERT INTO todo (id,create_date,due_date,complete,relation_id,role,content,recent_modifier)' +
        'VALUES (${id},${create_date},${due_date},${complete},${relation_id},${role},${content},${recent_modifier})', data)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: data.id,
                });
        })
        .catch(function (err) {
            console.log(err);
            return next(err);
        });
}

function deleteTodo(req,res,next){
    let todoId = req.params.id;
    console.log(todoId);
    if (!validator.isUUID(todoId)) {
        return next(new Error('todo format error'));
    }
    db.none('delete from todo where id = $1',todoId)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: todoId,
                });
        })
        .catch(function (err) {
            console.log(err);
            return next(err);
        })
}

function updateTodo(req,res,next){
    const fields = ["due_date", "complete", "role", "content"];
    req.body['id'] = req.params.id;
    if(!validator.isUUID(req.body.id)
    || !req.body.field || fields.indexOf(req.body.field) === -1){
        return res.status(400).json({
            status: "fail",
            message: "wrong data input"
        });
    }

    // check format base on field name
    if(req.body.field === "due_date"){
        let checkdate = Date.parse(req.body.value);
        if(isNaN(checkdate)){
            return res.status(400).json({
                status: "fail",
                message: "wrong data input"
            });
        }
        let duedate = new Date(req.body.value);
        if(duedate < (new Date())){
            return res.status(400).json({
                status: "fail",
                message: "wrong data input"
            });
        }
        req.body.value = duedate;
    }
    else if ((req.body.field === "complete" && (typeof req.body.value !== "boolean"))
    || (req.body.field === "role" && ROLES.indexOf(req.body.value) === -1)
    || (req.body.field === "content" && !(typeof req.body.value === 'string'))){
        return res.status(400).json({
            status: "fail",
            message: "wrong data input"
        });
    }
    // console.log(req.body);
    db.none('UPDATE todo SET ${field^} = ${value}, recent_modifier = ${userrole} WHERE id =${id}', req.body).then(() => {
        return res.status(200).json({
            status: "success",
            message: null
        });
    }).catch(err => {
        console.log(err);
        return res.status(400).json({
            status: "fail",
            message: err
        });
    });
}

function removeOneUser(req,res,next) {
    /** behaviour: based on email address, remove one user from a login table
     *              -- if it is student's email, we should also manually remove bio info from student information
     *              -- if it is staff's email, we should also manually remove bio info from staff information
     *  method: DELETE
     *  note: url should contains the target email address
     * */
    var username = req.params.username;
    if (!username) {
        res.status(400).json({
            status:'fail',
            message:'need username'
        });
        return;
    }
    // console.log(req);
    db.one('delete from login where username = $1 returning *',username)
        .then(function (data) {
            var result = data;
            acl.removeUserRoles(data.username, data.role);
            // console.log(result);
            if (result.role === "STUD") {
                removeOneStudent(result.id,res,next);
            } else {
                removeOneStaff(result.id,res,next);
            }
        }).catch(function (err) {
            return next(err);
    });
}

function removeOneStudent(stuID,res,next) {
    if (!stuID) {
        res.status(400).json({
            status:'fail',
            message:'need student id'
        });
        return;
    }
    db.result('delete from student where id = $1',stuID)
        .then(function (result) {
            console.log(result);
            res.status(200).json({
                status:'success',
                message:'Removed:'+result
            });
        }).catch(function (err) {
            return next(err);
        });
}

function removeOneStaff(staffID,res,next) {
    if (!staffID) {
        res.status(400).json({
            status:'fail',
            message:'need staff id'
        });
        return;
    }
    db.result('delete from staff where id = $1',staffID)
        .then(function (result) {
            console.log(result);
            res.status(200).json({
                status:'success',
                message:'Removed:'+result
            });
        }).catch(function (err) {
            return next(err);
    });
}

function removeOneBadge(req,res,next) {
    let studId = req.params.studId;
    let badgeId = req.params.badgeId;
    console.log('remove badge ',badgeId,' from ',studId);
    db.result('update student set badges = array_remove(badges,$1) where id = $2',[badgeId,studId])
        .then(function (result) {
            console.log('remeve one badge',result);
            res.status(200).json({
                status:'success',
                message:'Removed:'+result
            });
        }).catch(function (err) {
            console.log(err);
        return next(err);
    });
}

function addOneBadge(req,res,next) {
    let studId = req.params.studId;
    let badgeId = req.params.badgeId;
    console.log('add badge ',badgeId,' to ',studId);
    db.none('update student set badges = badges || \'{$1^}\' where id = $2',[badgeId,studId])
        .then(function (result) {
            console.log('add one badge',result);
            res.status(200).json({
                status:'success',
                message:'Removed:'+result
            });
        }).catch(function (err) {
            console.log(err);
        return next(err);
    });
}

function userFirstLoginResetCB(req, thenCallBack, catchCallBack) {
    db.none('update login set password = $1, isvisited = $2 where id = $3',
        [req.body.password, req.body.isvisited ,req.body.userID])
        .then(thenCallBack)
        .catch(function(err) { catchCallBack(err) });
}

function updateIsVistedCB(req, thenCallBack, catchCallBack) {
    db.none('update login set isvisited = $1 where id = $2',
        [req.body.isvisited ,req.body.userID])
        .then(thenCallBack)
        .catch(function(err) { catchCallBack(err) });
}

function getActivity(req,res,next){
    return db.any('SELECT * FROM activity WHERE relation_id = ${id}', req.params)
        .then(data => {
            console.log(data);
            if(data.length === 0) return res.status(200).json({
                status: 'success',
                message: [],
            });

            for(let i=0; i<data.length; i++){
                delete data[i].id;
                delete data[i].relation_id;
            }
            return res.status(200).json({
                status: 'success',
                message: data,
            });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json({
                status: 'fail',
                message: err,
            });
        });
}

function logActivity(req,res,next){
    var data = standardizeIn(req.body, 'activity');
    if(data instanceof Error){
        return res.status(400).json({
            status: 'fail',
            message: 'activity format error'
        });
    }
    db.none('INSERT INTO activity (id,create_date,type,hours,happi,detail,relation_id,tier,meet_date)' +
            'VALUES (${id},${create_date},${type},${hours},${happi},${detail},${relation_id},${tier},${meet_date})', data)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: data.id
                });
        })
        .catch(function (err) {
            console.log(err);
            return next(err);
    });
}

function isAllInfoReadyCB(userID, isStudent, thenCallBack, catchCallBack){
    let table = isStudent ? 'student' : 'staff';
    let cmd = 'SELECT count(*) FROM ' + table + ' WHERE id = ' + `'${userID}'`;
    db.one(cmd)
        .then((data) => {
            let res = data.count !== '0';
            thenCallBack(res);
        })
        .catch((err) => { catchCallBack(err)});
}

// utility functions used for this database
function standardizeIn(body,mode) {
    if(!body) {
        let error = new Error('no input to standardize');
        console.log(error);
        return error;
    }
    var out = {};
    var date = new Date();
    if (mode === 'student') {
        out['id'] = uuidv5(body.email, UUIDSEED);
        out['create_date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        out['contact'] = getcontactString(body,mode);
        out['bio'] = JSON.stringify(body);
        out['hours'] = '{0,0,0,0,0}';
        out['happi'] = '{0,0,0,0,0}';
        out['badges'] = '{"c1"}';
        out['gpa'] = '{"GPA_website":"","GPA_password":"","GPA_username":""}';
        out['wma'] = '{"WMA_day":"","WMA_time":"","WMA_location":""}';
        out['vom'] = '{"VoM_username":"","VoM_password":""}';
        out['wyz'] = '{"WyZ_username":"","WyZ_password":""}';
    }
    else if (mode === 'staff') {
        out['id'] = uuidv5(body.email, UUIDSEED);
        out['create_date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        out['contact'] = getcontactString(body,mode);
        out['bio'] = JSON.stringify(body);
        out['relation_id'] = '{}';
    }
    else if (mode === 'newuser') {
        if(!validator.isEmail(body.username) || ROLES.indexOf(body.userrole) === -1) {
            let error = new Error('newuser format error');
            console.log(error);
            return error;
        }
        out['id'] = uuidv5(body.username, UUIDSEED);
        out['username'] =body.username;
        out['role'] = body.userrole;
        out['password'] = body.password;
        out['isvisited'] = 0;
        out['nickname'] = '';
    }
    else if (mode === 'activity') {
        // console.log(body);
        if(!body.type || !(typeof body.type==='string') ||
            !body.hours || !(typeof body.hours==='number') ||
            !body.happi || !(typeof body.happi==='number') ||
            !body.tier || !(typeof body.tier==='string') ||
            !body.meet_date ||
            !body.detail.name || !(typeof body.detail.name==='string') ||
            !body.detail.describe || !(typeof body.detail.describe==='string') ||
            !body.detail.position || !(typeof body.detail.position==='string') ||
            !validator.isUUID(body.relation_id)
        ) {
            let error = new Error('activity format error');
            console.log(error);
            return error;
        }

        let checkdate = Date.parse(body.meet_date);
        if(isNaN(checkdate)){
            let error = new Error('activity format error');
            console.log(error);
            return error;
        }
        let meetdate = new Date(body.meet_date);
        if(meetdate > date){
            let error = new Error('activity format error');
            console.log(error);
            return error;
        }

        out['id'] = uuidv4();
        out['create_date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        out['meet_date'] = meetdate.getFullYear() + '-' + (meetdate.getMonth() + 1) + '-' + meetdate.getDate();
        // out['type'] = body.type.toLowerCase();
        // out['type'] = body.type.toUpperCase();
        out['type'] = body.type;
        out['hours'] = body.hours;
        out['happi'] = body.happi;
        out['tier'] = body.tier;
        out['detail'] = body.detail;
        // out['detail'] = {
        //     name: body.detail.name,
        //     describe: body.detail.describe,
        //     position: body.detail.position,
        // };
        out['relation_id'] = body.relation_id;
    }
    else if (mode === 'todo') {
        console.log(body);
        if(!body.due_date
        || !validator.isUUID(body.relation_id)
        || !body.role || ROLES.indexOf(body.role) === -1
        || !body.content || !(typeof(body.content) ==='string')) {
            let error = new Error('todo format error');
            console.log(error);
            return error;
        }
        let checkdate = Date.parse(body.due_date);
        if(isNaN(checkdate)){
            let error = new Error('todo format error');
            console.log(error);
            return error;
        }
        let duedate = new Date(body.due_date);
        console.log(duedate.toDateString(),date.toDateString());
        if(duedate < date){
            let error = new Error('todo format error');
            console.log(error);
            return error;
        }
        out['id'] = uuidv4();
        out['create_date'] = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        out['due_date'] = duedate.getFullYear() + '-' + (duedate.getMonth() + 1) + '-' + duedate.getDate();
        out['complete'] = false;
        out['relation_id'] = body.relation_id;
        out['role'] = body.role;
        out['content'] = body.content;
        out['recent_modifier'] = body.recent_modifier;
    }
    // ...more modes
    // console.log(out);
    return out;
}

function getcontactString(body,mode) {
    var out = {};
    out['id'] = uuidv5(body.email, UUIDSEED);
    out['phone'] = body.phone;
    out['email'] = body.email;
    out['wechat'] = body.wechat;
    out['skype'] = body.skype;
    out['name'] = body.family_name+' '+body.given_name;
    delete body.phone;
    delete body.email;
    delete body.wechat;
    delete body.skype;
    delete body.family_name;
    delete body.given_name;
    return JSON.stringify(out);
}

function filterTodo(todo) {
    var out = {}
    if (todo) {
        todo.forEach((data)=>{
            if (data.role === 'STUD') {
                if (!out[data.role]) {
                    out[data.role] = []
                }
                out[data.role].push(data)
            }
            else {
                if (!out['STAFF']) {
                    out['STAFF'] = []
                }
                out['STAFF'].push(data)
            }
        })
    }
    return out;
}
// not use yet
// function getoneFromLogin (req,res,next) {
//     // console.log(req.cookies);
//     console.log(req.params);
//     var username = req.params.username;
//     db.one('select * from login where username = $1',username)
//         .then(function(data){
//             console.log(data);
//             res.status(200).json(
//                 {
//                     status:'success',
//                     data:data,
//                     message:'Retrieved one record from login'
//                 }
//             );
//         }).catch(function (err) {
//         return next(err);
//     })
// }
// function getoneStudent(req,res,next) {
//     console.log(req.params);
//     // var stuID = parseInt(req.params.id);
//     // console.log(stuID);
//     var stuID = req.params.id;
//     db.one('select * from student where id = $1',stuID)
//         .then(function (data) {
//             res.status(200).json({
//                 status: 'success',
//                 data: data,
//                 message: 'Retrieved ONE student'
//             });
//         }).catch(function (err) {
//         console.log(err);
//         return next(err);
//     });
// }
// function getoneStaff(req,res,next) {
//     var staffID = req.params.id;
//     db.one('select * from staff where id = $1',staffID)
//         .then(function (data) {
//             res.status(200).json({
//                 status: 'success',
//                 data: data,
//                 message: 'Retrieved ONE staff'
//             });
//         }).catch(function (err) {
//         return next(err);
//     });
// }
// function fetchRole(req,res,path,levelcontrol,callback) {
//     db.one('select role from login where id = $1',req.who)
//         .then(function (data) {
//             console.log(data);
//             callback(null,data,path);
//         }).catch(function (err) {
//         // console.log(err);
//         // return next(err);
//         callback(err);
//     });
// }
// function getAllActivity(req,res,next){
//     db.task(t => {
//         return t.any('SELECT * FROM login')
//             .then(user => {
//                 return t.any('SELECT * from todo');
//             });
//     }) .then(events => {
//         // success
//         console.log(events);
//         res.end('success');
//     }) .catch(error => {
//         // error
//         console.log(error);
//         res.end('errors!');
//     });
// }
//
// function getActivityFromRelation(relation_id,callback) {
//     db.any('select * from activity where relation_id = &1',relation_id)
//         .then(function (data) {
//             console.log(data);
//             callback(null,data);
//         }).catch(function (err) {
//
//     });
// }